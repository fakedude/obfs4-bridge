# Base docker image
FROM debian:stable-slim

# Set to 101 for backward compatibility
ARG UID=101
ARG GID=101

LABEL maintainer="fakedude <fakedude@gmail.com>"

RUN groupadd -g $GID debian-tor && \
    useradd -m -u $UID -g $GID -s /bin/false -d /var/lib/tor debian-tor

RUN printf "deb http://deb.debian.org/debian stable-backports main\n" >> /etc/apt/sources.list.d/backports.list && \
    apt-get update && apt-get install -y \
    obfs4proxy tor tor-geoipdb libcap2-bin \
    --no-install-recommends -t stable-backports

# Allow obfs4proxy to bind to ports < 1024.
RUN setcap cap_net_bind_service=+ep /usr/bin/obfs4proxy

# Our torrc is generated at run-time by the script start-tor.sh.
RUN rm /etc/tor/torrc && \
    chown debian-tor:debian-tor /etc/tor && \
    chown debian-tor:debian-tor /var/log/tor

COPY start-tor.sh /usr/local/bin
COPY get-bridge-line /usr/local/bin

RUN chmod 0755 /usr/local/bin/start-tor.sh && \
    chmod 0755 /usr/local/bin/get-bridge-line

USER debian-tor

CMD [ "/usr/local/bin/start-tor.sh" ]
